-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 25-Out-2015 às 22:27
-- Versão do servidor: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `drone`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `questao`
--

CREATE TABLE IF NOT EXISTS `questao` (
`id_questao` int(11) NOT NULL COMMENT 'id da questao',
  `ds_questao` text COLLATE utf8_bin NOT NULL COMMENT 'descricao da questao',
  `dificuldade` varchar(30) COLLATE utf8_bin NOT NULL COMMENT 'dificuldade da questao',
  `resposta` int(11) NOT NULL COMMENT 'id do quadrado que responde a questao'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `rank`
--

CREATE TABLE IF NOT EXISTS `rank` (
`id_rank` int(11) NOT NULL COMMENT 'id do rank',
  `ds_rank` varchar(45) COLLATE utf8_bin NOT NULL COMMENT 'descricao do rank',
  `pontos` int(11) NOT NULL COMMENT 'pontos do rank'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `questao`
--
ALTER TABLE `questao`
 ADD PRIMARY KEY (`id_questao`);

--
-- Indexes for table `rank`
--
ALTER TABLE `rank`
 ADD PRIMARY KEY (`id_rank`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `questao`
--
ALTER TABLE `questao`
MODIFY `id_questao` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id da questao';
--
-- AUTO_INCREMENT for table `rank`
--
ALTER TABLE `rank`
MODIFY `id_rank` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id do rank';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
