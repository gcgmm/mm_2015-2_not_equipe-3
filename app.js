var arDrone = require('ar-drone');
var client  = arDrone.createClient();

//Velocidade vertical
client.config('Control:control_vz_max', 200);
client.config('Control:control_yaw', 40);

var Quagga = require('quagga');
var http = require('http').createServer(servidor);
var fs = require('fs');
var mysql      = require('mysql');
var perguntaHtml = '';
var resultadoHtml = '';
var pontuacaoHtml = 10;
var respostaObtida = '';
var respostaEsperada = '';
var respostaEsperadaHtml = '';
var redirecionaHtml = '';
var idQuestao = 3;
var pontuacao = 0;

function servidor(requisicao, resposta){
  var url = requisicao.url;
  var connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'root',
	password : '',
	database : 'drone'
  });
  
  //Conectando no banco
  connection.connect(function(err){
  if(err){
    console.log('Error connecting to Db');
	return;
  }
	//console.log('Connection established');
  });	
  
  if(url == "/proxima"){
	  idQuestao++;
	  resultadoHtml = '';
	  respostaObtida = '';
	  respostaEsperadaHtml = '';
	  redirecionaHtml = '';
  } 
		
  connection.query('SELECT * from questao WHERE id_questao = ' + idQuestao, function(err, rows, fields) {
  if (!err){
	for(var i in rows)  {
		perguntaHtml = rows[i].ds_questao;
		respostaEsperada = rows[i].resposta;
		dificuldade = rows[i].dificuldade;
	}	
  }else
	console.log('Error while performing Query.');
  });
  connection.end();
  //Fim da conexao
  
  if(url == '/verificar'){
    Quagga.decodeSingle({
    src: "http://localhost:8080",
    numOfWorkers: 0,  // Needs to be 0 when used within node 
    inputStream: {
        size: 640  // restrict input-size to be 800px in width (long-side) 
    },
    decoder: {
        readers: ["code_39_reader"] // List of active readers 
    },
    }, function(result) {
      if(result.codeResult) {
          //console.log("result", result.codeResult.code);
		  respostaObtida = result.codeResult.code;
		  respostaEsperadaHtml = respostaEsperada;
		  if(respostaObtida == respostaEsperada){
			resultadoHtml = 'green"> Acertoooouuuuu!!!!';
			if(dificuldade == "Facil"){
				pontuacaoHtml += 10;
			} else if(dificuldade == "Medio"){
				pontuacaoHtml += 20;
			} else{
				pontuacaoHtml += 50;
			}			
		  } else{
			resultadoHtml = 'red"> Errrroooouuuu!!!!';
			pontuacaoHtml = 0;
		  }
      } else {
          console.log("not detected");
      }
	});		
  }else if(url == '/decolar'){
	client.takeoff();
	
    client.after(5, function() {
      this.calibrate(0);
    });
  }else if(url == '/pousar'){
	client.land();
  }else if(url == '/subir'){
	client.up(0.5);
    
    client.after(1, function() {
      this.stop();
    });
  }else if(url == '/descer'){
	client.down(0.5);
    client.after(1, function() {
      this.stop();
    });
  }else if(url == '/virarDireita'){
	client.clockwise(0.5);    
    client.after(25, function() {
      this.stop();
    });
  }else if(url == '/virarEsquerda'){
    client.counterClockwise(0.5);    
    client.after(25, function() {
      this.stop();
    });	
  }else if(url == '/esquerda'){
    client.left(0.2);    
    client.after(5, function() {
      this.stop();
    });  
  }else if(url == '/atras'){
    client.back(0.2);
    client.after(1, function() {
      this.stop();
    });	  
  }else if(url == '/direita'){
    client.right(0.2);    
    client.after(5, function() {
      this.stop();
    });	     
  }else if(url == '/frente'){
    client.front(0.2);
    client.after(1, function() {
      this.stop();
    });
  }else if(url == '/deuRuim'){
    client.disableEmergency();
  }else if(url == '/tentarNovamente'){
    //Tentar novamente
  }  
  
  //sleep(5000);
  resposta.end(fs.readFileSync('telaTop.html') + /*redirecionaHtml +*/ fs.readFileSync('telaTop2.html') +
               perguntaHtml + fs.readFileSync('telaBotton.html') +
               resultadoHtml + fs.readFileSync('telaBotton2.html') +
               pontuacaoHtml + fs.readFileSync('telaBotton3.html') + 
               respostaObtida + fs.readFileSync('telaBotton4.html') +
               respostaEsperadaHtml + fs.readFileSync('telaBotton5.html'));
  resposta.writeHead(200);			   
};

http.listen(3000, function(){
  console.log("Servidor On-line");
});

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}